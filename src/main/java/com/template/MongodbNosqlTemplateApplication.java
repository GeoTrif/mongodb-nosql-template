package com.template;

import com.template.repository.SuperstarRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories(basePackageClasses = SuperstarRepository.class)
public class MongodbNosqlTemplateApplication {

    // Install MongoDB and MongoDB Compass and add the documents from resources/superstars.json.
    public static void main(String[] args) {
        SpringApplication.run(MongodbNosqlTemplateApplication.class, args);
    }
}
