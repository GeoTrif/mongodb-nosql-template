package com.template.controller;

import com.template.service.SuperstarService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SuperstarController {

    private static final String INDEX_PAGE_ENDPOINT = "/";
    private static final String INDEX_PAGE_NAME = "index";

    private static final String ANGELINA_JOLIE_ATTRIBUTE_NAME = "angelinaJolie";
    private static final String KEANU_REEVES_ATTRIBUTE_NAME = "keanuReeves";
    private static final String WILL_SMITH_ATTRIBUTE_NAME = "willSmith";
    private static final String GAL_GADOT_ATTRIBUTE_NAME = "galGadot";
    private static final String THE_ROCK_ATTRIBUTE_NAME = "theRock";

    private final SuperstarService superstarService;

    public SuperstarController(SuperstarService superstarService) {
        this.superstarService = superstarService;
    }

    @GetMapping(INDEX_PAGE_ENDPOINT)
    public String getIndexPage(Model model) {
        model.addAttribute(WILL_SMITH_ATTRIBUTE_NAME, superstarService.getSuperstars().get(0));
        model.addAttribute(ANGELINA_JOLIE_ATTRIBUTE_NAME, superstarService.getSuperstars().get(1));
        model.addAttribute(KEANU_REEVES_ATTRIBUTE_NAME, superstarService.getSuperstars().get(2));
        model.addAttribute(GAL_GADOT_ATTRIBUTE_NAME, superstarService.getSuperstars().get(3));
        model.addAttribute(THE_ROCK_ATTRIBUTE_NAME, superstarService.getSuperstars().get(4));

        return INDEX_PAGE_NAME;
    }
}
