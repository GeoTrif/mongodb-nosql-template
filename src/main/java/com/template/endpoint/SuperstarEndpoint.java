package com.template.endpoint;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.template.model.SuperStar;
import com.template.service.SuperstarService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SuperstarEndpoint {

    private static final String GET_SUPERSTARS_ENDPOINT = "/get-superstars";
    private static final String SAVE_SUPERSTAR_ENDPOINT = "/save-superstar";
    private static final String UPDATE_SUPERSTAR_ENDPOINT = "/update-superstar";
    private static final String DELETE_SUPERSTAR_ENDPOINT = "/delete-superstar";
    private static final String GET_SUPERSTAR_BY_NAME_ENDPOINT = "/get-superstar-by-name";

    private static final ObjectMapper MAPPER = new ObjectMapper();

    private final SuperstarService superstarService;

    public SuperstarEndpoint(SuperstarService superstarService) {
        this.superstarService = superstarService;
    }

    @GetMapping(GET_SUPERSTARS_ENDPOINT)
    public List<SuperStar> getSuperstars() {
        return superstarService.getSuperstars();
    }

    @GetMapping(GET_SUPERSTAR_BY_NAME_ENDPOINT)
    public SuperStar getSuperstarByName(@RequestParam String name) {
        return superstarService.getSuperstarByName(name);
    }

    @GetMapping(SAVE_SUPERSTAR_ENDPOINT)
    public void saveSuperstar(@RequestBody String superstarJson) throws JsonProcessingException {
        SuperStar superStar = MAPPER.readValue(superstarJson, SuperStar.class);
        superstarService.saveSuperstar(superStar);
    }

    @GetMapping(UPDATE_SUPERSTAR_ENDPOINT)
    public void updateSuperstar(@RequestParam String id, @RequestParam String newName) {
        superstarService.updateSuperstar(id, newName);
    }

    @GetMapping(DELETE_SUPERSTAR_ENDPOINT)
    public void deleteSuperstar(@RequestParam String id) {
        superstarService.deleteSuperstar(id);
    }
}