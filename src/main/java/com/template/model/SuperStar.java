package com.template.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = "superstar")
public class SuperStar implements Serializable {

    @Id
    private String id;
    private String name;
    private String age;
    private String city;
    private int numberOfFollowers;
    private int numberOfLikes;
    private int numberOfPhotos;

    public SuperStar() {
    }

    public SuperStar(String id, String name, String age, String city, int numberOfFollowers, int numberOfLikes, int numberOfPhotos) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.city = city;
        this.numberOfFollowers = numberOfFollowers;
        this.numberOfLikes = numberOfLikes;
        this.numberOfPhotos = numberOfPhotos;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getNumberOfFollowers() {
        return numberOfFollowers;
    }

    public void setNumberOfFollowers(int numberOfFollowers) {
        this.numberOfFollowers = numberOfFollowers;
    }

    public int getNumberOfLikes() {
        return numberOfLikes;
    }

    public void setNumberOfLikes(int numberOfLikes) {
        this.numberOfLikes = numberOfLikes;
    }

    public int getNumberOfPhotos() {
        return numberOfPhotos;
    }

    public void setNumberOfPhotos(int numberOfPhotos) {
        this.numberOfPhotos = numberOfPhotos;
    }

    @Override
    public String toString() {
        return "SuperStar{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age='" + age + '\'' +
                ", city='" + city + '\'' +
                ", numberOfFollowers=" + numberOfFollowers +
                ", numberOfLikes=" + numberOfLikes +
                ", numberOfPhotos=" + numberOfPhotos +
                '}';
    }
}
