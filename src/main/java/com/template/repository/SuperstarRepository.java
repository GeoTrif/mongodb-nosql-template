package com.template.repository;

import com.template.model.SuperStar;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SuperstarRepository extends MongoRepository<SuperStar, String> {
    SuperStar findByName(String name);
}