package com.template.service;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import com.template.model.SuperStar;
import com.template.repository.SuperstarRepository;
import org.bson.Document;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SuperstarService {

    private static final String MONGODB_CONNECTION_STRING = "mongodb://127.0.0.1:27017";
    private static final String MONGODB_DATABASE_NAME = "mongodbTemplate";
    private static final String SUPERSTAR_COLLECTION_NAME = "superstar";
    private static final String NEW_NAME_KEY_ATTRIBUTE = "name";
    private static final String SET_UPDATE_OPERATOR = "$set";

    private MongoDatabase db;
    private MongoClient mongo;
    private MongoCollection<Document> superstarCollection;

    private final SuperstarRepository superstarRepository;

    public SuperstarService(SuperstarRepository superstarRepository) {
        this.superstarRepository = superstarRepository;
        setUpMongoDBConnections();
    }

    public List<SuperStar> getSuperstars() {
        return superstarRepository.findAll();
    }

    public SuperStar getSuperstarByName(String superstarName) {
        return superstarRepository.findByName(superstarName);
    }

    public void saveSuperstar(SuperStar superStar) {
        superstarRepository.save(superStar);
    }

    public void updateSuperstar(String key, String newName) {
        BasicDBObject getSuperstarQuery = new BasicDBObject();
        getSuperstarQuery.get(key);

        BasicDBObject newNameQuery = new BasicDBObject();
        newNameQuery.put(NEW_NAME_KEY_ATTRIBUTE, newName);

        BasicDBObject updateSuperstar = new BasicDBObject();
        updateSuperstar.put(SET_UPDATE_OPERATOR, newNameQuery);

        superstarCollection.updateOne(getSuperstarQuery, updateSuperstar);
    }

    public void deleteSuperstar(String key) {
        BasicDBObject getSuperstarQuery = new BasicDBObject();
        getSuperstarQuery.get(key);

        superstarCollection.findOneAndDelete(getSuperstarQuery);
    }

    private void setUpMongoDBConnections() {
        mongo = MongoClients.create(MONGODB_CONNECTION_STRING);
        db = mongo.getDatabase(MONGODB_DATABASE_NAME);
        superstarCollection = db.getCollection(SUPERSTAR_COLLECTION_NAME);
    }
}
